# PHP

This test is designed to get an idea about coding style and competency. Please complete the following tasks to the
best of your abilities.

1. Create a new branch from master and call it dev/<your-name>
2. Provide SQL that will create the following structure.
    1. Table to store user information
    2. Table to store product information
    3. Necessary tables to store information about user purchases
3. Simple view to list, add, edit products
4. Simple view to list users.
5. Commit your work to the branch created in step 1 and create a pull request.

# JavaScript / Angular

To demonstrate your proficiency with modern JavaScript frameworks, please create a new Angular project using angular 12 (or latest) that has at least one route, integrates a free API of your choice (for example https://openweathermap.org/), and contains at least one reusable component. Please put a little time into making it look nice. BONUS: Create a NativeScript app using Angular that integrates the same API.

## Other requirements

* Before creating a pull request you will be require access. Please send us your bitbucket user.
* Your code should work with MySQL 5.6, PHP 7.X and Apache 2.4 and Linux Server.
* Your code should be formatted following PSR-2 standards.
* When possible don't commit entire libraries and use a package manager to bring that code during installation.      
* Please send any instructions on how to run and test your code.
